var mymap = L.map('mapid').setView([-41.288764, 174.777224], 13); //sets the map object

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'sk.eyJ1IjoiYW5kcmV3cmp0IiwiYSI6ImNqdmRmOXc1ZjFmc3A0NG5tYTN6OXgxdjQifQ._9SBCS6imUJyQqOStBERzQ'
}).addTo(mymap); //inatiates the map object using the accesstoken

locarray = []; //array to store all the objects of previous searches

//constructor function used for previous searches holding the latitude longdatutide and the name
function oldlocations(name, llarray) {
    this.locname = name;
    this.loclatlng = llarray;
}
//starting function called by the submit button in the HTML checks so see if the input is empty telling user to enter something otherwise calls the geocode to get the lat and lng
getLoc = function() {
    locval = document.getElementById("loc").value;
    if (locval === "") {
        alert("Please enter a city");
    } else {
        fetch("https://www.mapquestapi.com/geocoding/v1/address?key=c3BoNZpsE3s7gnlZJ5nKioYEYrAJtNN6&inFormat=kvp&outFormat=json&location=" + locval + "+NZ&thumbMaps=false") //api with hardcoded key
            .then(response => response.json())
            .then(json => setnewmap(json));
    }
}

//function called by getloc once request has go through
setnewmap = function(result) {
    if (result.info.statuscode === 0) { //checks if there is no error 
        locinfo = result.results[0].locations[0].displayLatLng;
        locn = result.results[0].locations[0].adminArea5;
        ccheck = result.results[0].locations[0].adminArea1; //sets all variables used for checking and storing for later use
        locvala = document.getElementById("loc").value;
        locinfo1 = locn.toUpperCase();
        locvala1 = locvala.toUpperCase(); //sets to uppercase for check to make sure no errors will occur with different case
        Len = locarray.length;
        exists = false;
        for (i = 0; i < Len; i++) { //for loop checking to make sure if the search has already occured
            if (locarray[i].locname === locn) {
                exists = true;
            };
        }
        if (locinfo1 == locvala1 && ccheck == "NZ" && exists == false) { //if all conditions are met to function will set new map and make a new object and send it to the array
            var setm = [locinfo.lat, locinfo.lng];
            var locn = new oldlocations(locn, setm);
            setmap(setm);
            locarray.push(locn);
            oldlocarea();
        } else { //errors if something happened cause this part is a critical component to the rest i choose to do it as a alert because no other functions can occur if this doesnt happen
            if (exists == true) {
                alert("City already entered");
            } else {
                alert("City does not exit in NZ");
            }
        };
    } else {
        alert("Something went wrong trying to get the map try again later");
    }
}

setmap = function(inc) { //function that changed the map to where the new location is and also begins the retrival of sun and weather data. this is called when a new location is made but also when a old location is checked
    mymap.setView(inc, 13);
    getsun(inc);
    getWeather(inc);
}

oldlocarea = function() { //this function formats the HTML area for previous searches it first clears the area of anything remaining then set it to a unordered list that is iterating over the locations array making a new clickable function every time
    var node = document.getElementById("oldlocplace");
    node.innerHTML = "";
    node.innerHTML += "<h3>Previous Searches</h3>";
    node.innerHTML += "<ul id oldlist>";
    Len = locarray.length;
    for (i = 0; i < Len; i++) {
        node.innerHTML += "<li id=oldones onclick=setmap([" + locarray[i].loclatlng + "])>" + locarray[i].locname + "</li>";
    }
    node.innerHTML += "</ul>";
}

getsun = function(info) { //fetch into cUrl to retrvie the sun data
    fetch("sun.php?lat=" + info[0] + "&lng=" + info[1])
        .then(response => response.json())
        .then(json => showSun(json));
}

showSun = function(suninfo) { //response from retriveal of the sun data if an error occurs it will inform the user that something has happened
    if (suninfo.status === "INVALID_REQUEST") {
        document.getElementById("suninfo").innerHTML = "<h2 id=suns> Something went wrong getting the sunrise and sunset please try again</h2>";
    } else {
        document.getElementById("suninfo").innerHTML = "<h2 id=suns>Sun rises at " + suninfo.results.sunrise + " and sets at " + suninfo.results.sunset + "</h2>";
    }
}

var request; //request for AJAX started

getWeather = function(newll) { //usual AJAX request to collect weather data
    request = new XMLHttpRequest();
    url = "weather.php?lat=" + newll[0] + "&lng=" + newll[1]; //cUrl PHP
    request.open("GET", url);
    request.onreadystatechange = function() {
        if (request.readyState == 4) {
            if (request.status == 200) {
                result = request.responseXML; //response is brought back as XML
                displayInfo(result);
            } else {
                document.getElementById("winfo").innerHTML = "<h2>Something went wrong getting the tempeture data please try again later</h2>"; //if something goes wrong the field used to hold the weather data is changed to inform the user something has happened
            }
        }
    };
    request.send();
}

displayInfo = function(xmlDoc) { //callback for the AJAX parses the XML and makes the display
    w = xmlDoc.getElementsByTagName("weather")[0];
    t = xmlDoc.getElementsByTagName("temperature")[0];
    document.getElementById("winfo").innerHTML = "<h2 id=wstuff>The weather is " + w.getAttribute('value') + " Lowest Tempeture is " + t.getAttribute('min') + "&#8451 Max tempeture is " + t.getAttribute('max') + "&#8451</h2>";
}

var input = document.getElementById("loc"); //calls the input field function


input.addEventListener("keyup", function(event) { //sets the input so you can click enter as well as clicking submit

    if (event.keyCode === 13) {

        event.preventDefault();

        document.getElementById("bt").click();
    }
});