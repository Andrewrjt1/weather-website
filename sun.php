<?php

	$lat = $_GET["lat"];//latatudide and longdatide sent through from the javascript
	$lng = $_GET["lng"];

	$url = "https://api.sunrise-sunset.org/json?lat=". $lat ."&lng=" . $lng . "&date=today";//API called set as JSON
    //default return type is json
  	$process = curl_init($url);
  
    //curl_setopt($process, CURLOPT_SSL_VERIFYPEER,false ); called due to SSL not working well  home computer
    curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);  
  	$return = curl_exec($process);

  	echo $return;//sends JSON to javascript
  
  
  	curl_close($process);