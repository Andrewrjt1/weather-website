<?php

	$lat = $_GET["lat"];//the passed in latidude and longtadude 
	$lng = $_GET["lng"];

	$url = "https://api.openweathermap.org/data/2.5/weather?lat=" . $lat . "&lon=" . $lng . "&appid=7338ce5d9acf706ebea9e914c3e5e367&units=metric&mode=xml";//calls the API with hardcoded key passing is latatidude and longtidie
	//explicitly ask for response as xml
  	$process = curl_init($url);
  
    //curl_setopt($process, CURLOPT_SSL_VERIFYPEER,false ); this is something i called because SSL works funny on my home computer
  	curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
  	$return = curl_exec($process);

	$xml=simplexml_load_string($return) or die("Error: Cannot create object");//document sent as a XML
	header('Content-type: text/xml');
    echo $xml->asXML();


  
  	curl_close($process);